const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const userRoutes = require('./routes/user')
const productRoutes = require('./routes/product')
const orderRoutes = require('./routes/order')

const PORT = process.env.PORT || 4000;

const app = express();

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())
mongoose.connect('mongodb+srv://admin:car5412364@zuittbootcamp.5mphj.mongodb.net/Capstone-3?retryWrites=true&w=majority', 
{
	useNewUrlParser:true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error",console.error.bind(console,"Connection Error"))

db.once('open',() => console.log('Connected to the cloud database'))


app.use('/users',userRoutes)
app.use('/products',productRoutes)
app.use('/orders',orderRoutes)

app.listen(PORT, () => console.log(`Server is running at port ${PORT}`))