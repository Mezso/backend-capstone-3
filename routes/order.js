const express= require('express');
const router = express.Router()
const orderController = require('../controllers/order')
const auth = require('../auth')

router.post('/addCart',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false){
	
	orderController.createOrder(req.body).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send(false)
	}
	
})

router.get('/List',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	orderController.detailsOrder({userId : userData.id}).then(resultFromController => res.send(resultFromController))
	
	
})
router.post("/history",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
		orderController.OrderHistory(req.body).then(resultFromController => res.send(resultFromController))
	
	
})



router.get('/active',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	orderController.findOrder(userData.ifwd).then(resultFromController => res.send(resultFromController))
})
module.exports = router;