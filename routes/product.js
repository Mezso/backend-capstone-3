const express= require('express');
const router = express.Router()
const productController = require('../controllers/product')
const auth = require('../auth')


router.get('/activeProduct',(req,res) =>{
	productController.retrieveAll().then(resultFromController => res.send(resultFromController))
})

router.get('/:productId',(req,res)=>{
	productController.retrieveOne(req.params).then(resultFromController=>res.send(resultFromController))
})
router.post("/create",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Invalid credential")
	}
	
})
// update product details
router.post("/:productId/update",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		productController.updateProduct(req.params,req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Invalid credential")
	}
	
})
// Archive
router.put('/:productId/archive',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		productController.archieveProduct(req.params,req.body).then(resultFromController=> res.send(resultFromController))
	} else {
		res.send(false)
	}
	
})
router.put('/:productId/activate',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		productController.activateProduct(req.params,req.body).then(resultFromController=> res.send(resultFromController))
	} else {
		res.send(false)
	}
	
})

module.exports = router;