const express= require('express');
const router = express.Router()
const userController = require('../controllers/user')
const auth = require('../auth')

router.post('/register',(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController))
})

router.post('/login',(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=> res.send(resultFromController))
})

router.post('/createOrder',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false){
		let data = {
		userId: req.body.userId,
		productId: req.body.productId
	}
	

	userController.createOrder(data).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send('you cannot order')
	}
	
})
// retrieve user order
// router.get('/order',auth.verify, (req,res)=>{
// 	const userData = auth.decode(req.headers.authorization)
// 	userController.retrieveUserOrder(req.body).then(resultFromController=> res.send(resultFromController))
// })

// get all user by admin
router.get('/allUser',auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		userController.getAllUser(req.body).then(resultFromController=> res.send(resultFromController))
	}
	else{
		res.send('invalid credential')
	}
})
// change any person into admin
router.put('/:userId/setAsAdmin',(req,res) =>{
	userController.setAsAdmin(req.params,req.body).then(resultFromController=> res.send(resultFromController))
})


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});



router.get("/order", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		userController.getOrder({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

router.get('/aProduct',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
	userController.retrieve(req.body).then(resultFromController => res.send(resultFromController))
}
else{
		res.send('invalid credential')
}
})



router.post('/history/',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false){
		let data = {
		userId: req.body.userId,
		productId: req.body.productId
	}
	

	userController.archiveOrder(data).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send('you cannot order')
	}
	
})
router.put('/archivehehe/',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false){
		let data = {
		userId: req.body.userId,
	}
	

	userController.archivehehe(data).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send('you cannot order')
	}
	
})


router.get("/trial", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		userController.trialdata({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

module.exports = router;