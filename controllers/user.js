const User = require('../models/user')
const Product = require('../models/product')
const bcrypt = require('bcrypt')
const auth =require('../auth')

module.exports.registerUser = (reqBody) =>{
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo:reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){

				return {access:auth.createAccessToken(result)}

			} else{
				return false
			}
		}
	}) 
}

module.exports.createOrder = async (data) =>{

	let isUserOrder = await User.findById(data.userId).then(user =>{
		user.Order.push({
			productId: data.productId
		})

		return user.save().then((user,error)=>{
			if(error){
				return false
			}
			else {
				return user
			}
		})
	})

	let isProductOrder = await Product.findById(data.productId).then( product =>{
		product.Order.push({
			userId: data.userId
		})
		return product.save().then((product,error)=>{
			if(error){
				return false
			}
			else {
				return product
			}
		})


	})
	if(isUserOrder && isProductOrder){
			return true
	} else {
		return false
		}
	
}

module.exports.retrieveUserOrder = async (user, reqBody) =>{
	if(user.id == reqBody.id){
		return User.findOne({_id:reqBody.id}).then(result =>{
			if(result == null){
				return 'no order'
			}
			else {
				return result.Order
			}
		})
	} 
	else {
		return 'invalid Credential'
	}
	
}

module.exports.getAllOrder = () => {
	return User.find({}).then(result =>{
		return result.Order
	})
}

module.exports.setAsAdmin = (reqParams,reqBody) =>{
	let setAsAdmin = {
		isAdmin : reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId,setAsAdmin).then((user,error)=>{
		if(error){
			return false
		}
		else {
			return true
		}
	})
}

module.exports.getAllUser = () =>{
	return User.find({}).then(result =>{
		return result
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {


		result.password = "";


		return result;

	});

};

module.exports.getOrder = (data) => {
	return User.findById(data.userId).then(result => {





		return result.Order;

	});

};
module.exports.archiveOrder = async (data) =>{

	let isUserOrder = await User.findById(data.userId).then(user =>{
		user.Order.pop()

		return user.save().then((user,error)=>{
			if(error){
				return false
			}
			else {
				return user
			}
		})
	})

	let isProductOrder = await Product.findById(data.productId).then( product =>{
		product.Order.pop()
		return product.save().then((product,error)=>{
			if(error){
				return false
			}
			else {
				return product
			}
		})


	})
	if(isUserOrder){
			return true
	} else {
		return false
		}

}
module.exports.archivehehe = async (data) =>{



	let isUserOrder =  User.findByIdAndUpdate(data.userId,{isActive:false}).then(user =>{

		user.Order.map(product =>{
			isActive: false
		})

		return user.save().then((user,error)=>{
			if(error){
				return false
			}
			else {
				return user
			}
		})
	})

	if(isUserOrder){
			return isUserOrder
				} else {
		return false
		}

}
module.exports.trialdata = (data) => {
	return User.findById(data.userId).then(result => {





		return result.Order.isActive;

	});

};
module.exports.retrieve = () => {
	return Product.find({}).then(result =>{
		return result
	})
} 