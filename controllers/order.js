const User = require('../models/user')
const Product = require('../models/product')
const Order = require('../models/order')
const bcrypt = require('bcrypt')
const auth =require('../auth')

module.exports.createOrder = async (data) =>{

		let isUserOrder = await User.findById(data.userId).then(user =>{
		user.Order.push({
			productId: data.productId
		})

		return user.save().then((user,error)=>{
			if(error){
				return false
			}
			else {
				return user
			}
		})
	})

	let isProductOrder = await Product.findById(data.productId).then( product =>{
		product.Order.push({
			userId: data.userId
		})
		return product.save().then((product,error)=>{
			if(error){
				return false
			}
			else {
				return product
			}
		})


	})
	if(isUserOrder && isProductOrder){
			return true
	} else {
		return false
		}
	
	
}

module.exports.detailsOrder = async (data) =>{

		return Order.findById(data.orderId).then(result => {

					return result;

			});

		
}
module.exports.OrderHistory = (reqBody) =>{
	let newProduct = new Order({
		userId : reqBody.userId,
		productId: reqBody.productId
	})

	return newProduct.save().then((course,error) =>{
		if(error){
			return false
		}
		else {
			return true
		}
	})

}



module.exports.findOrder = (data) => {
	return Order.find(data.orderId).then(result =>{
		return result
	})
}