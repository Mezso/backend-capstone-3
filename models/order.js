const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({

		userId:{
			type:String,
			required: [true, 'User ID is required']
		},
		productId: {
			type :String,
			required: [true,'product Id is required']
		},
		purchasedOn:{
			type: Date,
			default: new Date()
		},
		isActive:{
			type : Boolean,
			default: true
		}
		
	
	
})

module.exports = mongoose.model('Order',orderSchema)